import urllib

import requests
import time


def get_offers(input_string):
    print(input_string)

    if input_string.lower() == "comprigo":
        return {
            "attachments": [
                {
                    "fallback": ":moneybag:",
                    "image_url": "https://4.bp.blogspot.com/-mUc83QOddxI/WeENjLn7njI/AAAAAAAAAbY/yqqAzY0DYRMHujP-W9Ip5ROJcoE87uIygCLcBGAs/s1600/gold-1F4B0.png",
                    "color": "#0078BF"
                }
            ]
        }
    elif input_string.lower() in ["hackathon","hacking"]:
        return {
            "attachments": [
                {
                    "fallback": ":robot_face:",
                    "image_url": "https://media3.giphy.com/media/M3o3fL9nnxG4o/giphy.gif",
                    "color": "#0078BF"
                }
            ]
        }


    base_url = "https://comprigo-web-search.herokuapp.com/v2/search/products"
    locale = "de"
    url = base_url + "/" + locale
    query_body = {
        "query": {
            "keywords": input_string

        },
        "config": {
            "partner_id": "1262",
            "uid": "5315b663e4b0f9d5fec9a696",
            "api_key": "5512a2e623819667"
        }
    }
    r = requests.post(url, json=query_body)
    print(r.status_code, r.reason)

    if r.json()['foundOffersCount'] == 0:
        return {"text": "\n I'm sorry, I could not find any offers for \"" + input_string + "\" :disappointed: \n"}

    result = {
        "text": "\n Your products results for \"" + input_string + "\" \n View all results at https://www.comprigo.de/produkte/" + urllib.parse.quote_plus(
            input_string.replace(' ', '-'))
    }

    attachments = []

    for offer in r.json()["FoundOffers"]["OfferList"][:5]:
        link = offer.get("affiliate_link", "")
        link = link.replace("$subID", "P" + locale.upper() + "6666" + "comprigobuy")
        attachment = {
            "fallback": offer.get('product_name', ''),
            "color": "#36a64f",
            "author_name": offer.get('brand', ''),
            "title": offer.get("product_name", ""),
            "title_link": link,
            "fields": [{
                "title": "Price",
                "value": offer.get('product_price_display', ''),
                "short": True
            }, {
                "title": "Shop",
                "value": offer.get('merchant_name', ''),
                "short": True
            }],
            "thumb_url": offer.get('product_image', ''),
            "footer": "comprigo",
            "ts": int(time.time())
        }

        attachments.append(attachment)

    # print(json.dumps(r.json(), indent=4, sort_keys=True))
    result["attachments"] = attachments

    return result

# if __name__ == "__main__":
#     get_offers("SAmsung Galaxy S8")
