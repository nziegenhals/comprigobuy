import requests
import json
from flask import Flask, request, jsonify
import re
import time


def get_offers(domain):
    domain = domain.lower()
    endpoint = "http://external-rules.comprigo.com/v1/rules/6666/" + domain + "?type=all&locale=de&subid=comprigobuy"
    r = requests.get(endpoint, auth=('avast', 'GFNkjRVpy3p2h'))
    if (r.status_code == 204):
        return {
            "text": "\n I'm sorry, I could not find any coupons for the domain " + domain + " :disappointed:\n"
        }

    data = json.loads(r.content)
    if not "coupons" in data:
        return {
            "text": "\n I'm sorry, I could not find any coupons for the domain " + domain + " :disappointed:\n"
        }

    coupons = data["coupons"]

    result = {
        "text": "\n Your coupon results for domain: " + domain + " \n"
    }

    attachments = []

    for coupon in coupons:
        fields = []
        if (coupon.get('code', '') != None):
            fields.append({
                "title": "Code",
                "value": coupon.get('code', ''),
                "short": True
            })
        if (coupon.get('value', '') != None):
            fields.append({
                "title": "Value",
                "value": coupon.get('value', ''),
                "short": True
            })
        attachment = {
            "fallback": coupon.get('name', ''),
            "color": "#36a64f",
            "title": coupon.get("name", ""),
            "title_link": coupon.get("deep_link", ""),
            "footer": "comprigo",
            "ts": int(time.time())
        }
        if (len(fields) > 0):
            attachment["fields"] = fields

        attachments.append(attachment)

    result["attachments"] = attachments
    return result

    # print get_offers('lidl.de')
