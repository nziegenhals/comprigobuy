from flask import Flask, request, jsonify
import offers
import re
import coupons

app = Flask(__name__)


@app.route('/', methods=['POST'])
def index():
    if request.method == 'POST':
        regex = re.compile('^\s*([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}\s*$', re.M | re.I)
        input_string = request.form.get('text', '')
        if request.form.get('token', '') != "1kBcB12YztwiiqNXypF5lpTh":
            return jsonify({"text": "\n I'm sorry, you're not allowed to do that :disappointed:"})

        if (regex.match(input_string)):
            output = coupons.get_offers(input_string)
        else:
            output = offers.get_offers(input_string)

        return jsonify(output)

        # return request.data


if __name__ == "__main__":
    app.run()
